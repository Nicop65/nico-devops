# PAULOU-Devops

# TP-Base de la mise en réseau //Network

```bash
gcloud auth list
```
Cette commande affiche la liste des comptes Google Cloud authentifiés sur la machine locale, indiquant quel compte est actuellement actif pour les opérations gcloud.
 
```bash
gcloud config list project
```
Cette commande affiche le projet Google Cloud actuellement configuré et utilisé par les commandes gcloud.
 
```bash
gcloud config set compute/zone "europe-west4-b"
export ZONE=$(gcloud config get compute/zone)
```
La première ligne configure la zone de calcul par défaut à "europe-west4-b" pour les futures opérations gcloud, tandis que la deuxième ligne stocke cette zone configurée dans une variable d'environnement ZONE sur le shell actuel.
 
```bash
gcloud config set compute/region "europe-west4"
export REGION=$(gcloud config get compute/region)
```
La première ligne définit la région de calcul par défaut à "europe-west4" pour les futures opérations gcloud, tandis que la deuxième ligne stocke cette région configurée dans une variable d'environnement REGION sur le shell actuel.
 
```bash 
gcloud compute networks create taw-custom-network --subnet-mode custom
```
Cette commande crée un nouveau réseau VPC personnalisé nommé "taw-custom-network" dans Google Cloud avec un mode de sous-réseau configuré sur "custom", permettant la création manuelle de sous-réseaux avec des plages d'adresses IP spécifiques.
 
```bash
gcloud compute networks subnets create subnet-europe-west4 \
   --network taw-custom-network \
   --region europe-west4 \
   --range 10.0.0.0/16
```
Cette commande crée un sous-réseau nommé "subnet-europe-west4" dans le réseau VPC "taw-custom-network", spécifié pour la région "europe-west4", avec une plage d'adresses IP de "10.0.0.0/16".
 
```bash
gcloud compute networks subnets create subnet-us-east1 \
   --network taw-custom-network \
   --region us-east1 \
   --range 10.1.0.0/16
```
Cette commande crée un sous-réseau nommé "subnet-us-east1" dans le réseau VPC "taw-custom-network", localisé dans la région "us-east1", avec une plage d'adresses IP de "10.1.0.0/16".
 
```bash
gcloud compute networks subnets create subnet-us-west1 \
   --network taw-custom-network \
   --region us-west1 \
   --range 10.2.0.0/16
```
Cette commande crée un sous-réseau nommé "subnet-us-west1" dans le réseau VPC "taw-custom-network", situé dans la région "us-west1", avec une plage d'adresses IP de "10.2.0.0/16".
 
```bash
gcloud compute networks subnets list \
   --network taw-custom-network
```
Cette commande liste tous les sous-réseaux créés au sein du réseau VPC "taw-custom-network", fournissant des détails tels que leurs noms, régions, et plages d'adresses IP.
 
```bash
gcloud compute firewall-rules create nw101-allow-http \
--allow tcp:80 --network taw-custom-network --source-ranges 0.0.0.0/0 \
--target-tags http
```
Cette commande crée une règle de pare-feu nommée "nw101-allow-http" dans le réseau VPC "taw-custom-network", autorisant le trafic TCP sur le port 80 (HTTP) depuis toutes les adresses IP (0.0.0.0/0) vers les instances marquées avec le tag "http".
 
```bash
gcloud compute firewall-rules create "nw101-allow-icmp" --allow icmp --network "taw-custom-network" --target-tags rules
```
Cette commande crée une règle de pare-feu nommée "nw101-allow-icmp" dans le réseau VPC "taw-custom-network", autorisant le trafic ICMP (utilisé pour le ping et d'autres diagnostics réseau) vers les instances marquées avec le tag "rules".
 
```bash
gcloud compute firewall-rules create "nw101-allow-internal" --allow tcp:0-65535,udp:0-65535,icmp --network "taw-custom-network" --source-ranges "10.0.0.0/16","10.2.0.0/16","10.1.0.0/16"
```
Cette commande crée une règle de pare-feu nommée "nw101-allow-internal" dans le réseau VPC "taw-custom-network", autorisant tout le trafic TCP, UDP et ICMP au sein des plages d'adresses IP spécifiées, facilitant ainsi la communication interne entre les instances situées dans ces sous-réseaux.
 
```bash
gcloud compute firewall-rules create "nw101-allow-ssh" --allow tcp:22 --network "taw-custom-network" --target-tags "ssh"
```
Cette commande crée une règle de pare-feu nommée "nw101-allow-ssh" dans le réseau VPC "taw-custom-network", autorisant le trafic TCP sur le port 22 (utilisé pour SSH) vers les instances marquées avec le tag "ssh".
 
```bash
gcloud compute firewall-rules create "nw101-allow-rdp" --allow tcp:3389 --network "taw-custom-network"
```
Cette commande crée une règle de pare-feu nommée "nw101-allow-rdp" dans le réseau VPC "taw-custom-network", autorisant le trafic TCP sur le port 3389, qui est utilisé pour le protocole de bureau à distance (RDP), permettant ainsi les connexions RDP aux instances du réseau.
 
```bash
gcloud compute instances create us-test-01 \
--subnet subnet-europe-west4 \
--zone europe-west4-b \
--machine-type e2-standard-2 \
--tags ssh,http,rules
```
Cette commande crée une instance de machine virtuelle (VM) nommée "us-test-01" dans la zone "europe-west4-b", avec le type de machine "e2-standard-2". Elle est connectée au sous-réseau "subnet-europe-west4" et possède des tags "ssh", "http" et "rules", permettant l'application des règles de pare-feu correspondantes pour autoriser le trafic SSH, HTTP et d'autres règles spécifiées par "rules".
 
```bash
gcloud compute instances create us-test-02 \
--subnet subnet-us-east1 \
--zone us-east1-d \
--machine-type e2-standard-2 \
--tags ssh,http,rules
```
Cette commande crée une instance de machine virtuelle nommée "us
 
-test-02" dans la zone "us-east1-d" avec le type de machine "e2-standard-2". Elle est connectée au sous-réseau "subnet-us-east1" et possède des tags "ssh", "http" et "rules", permettant l'application des règles de pare-feu correspondantes pour autoriser le trafic SSH, HTTP et d'autres règles spécifiées par "rules".
 
```bash
gcloud compute instances create us-test-03 \
--subnet subnet-us-west1 \
--zone us-west1-c \
--machine-type e2-standard-2 \
--tags ssh,http,rules
```
Cette commande crée une instance de machine virtuelle nommée "us-test-03" dans la zone "us-west1-c" avec le type de machine "e2-standard-2". Elle est connectée au sous-réseau "subnet-us-west1" et possède des tags "ssh", "http" et "rules", permettant l'application des règles de pare-feu correspondantes pour autoriser le trafic SSH, HTTP et d'autres règles spécifiées par "rules".
 
```bash
ping -c 3 <us-test-02-external-ip-address> // mettre l'@IP externe
```
Cette commande envoie trois paquets ICMP à l'adresse IP externe de l'instance de machine virtuelle "us-test-02". Remplacez "<us-test-02-external-ip-address>" par l'adresse IP externe réelle de l'instance pour que la commande fonctionne correctement.
 
```bash
ping -c 3 <us-test-03-external-ip-address> // mettre l'@IP externe
```
Cette commande envoie trois paquets ICMP à l'adresse IP externe de l'instance de machine virtuelle "us-test-03". Remplacez "<us-test-03-external-ip-address>" par l'adresse IP externe réelle de l'instance pour que la commande fonctionne correctement.
 
```bash
ping -c 3 us-test-02.us-east1-d
```
Cette commande ping est destinée à l'adresse "us-test-02.us-east1-d", qui semble être une tentative d'adressage utilisant un format incorrect pour une adresse IP ou un nom de domaine. Pour exécuter la commande ping avec succès, vous devez spécifier l'adresse IP ou le nom d'hôte correct de l'instance cible dans la région us-east1-d.
 
```bash
sudo apt-get update
```
Cette commande met à jour la liste des paquets disponibles dans les référentiels configurés sur votre système.
 
```bash
sudo apt-get -y install traceroute mtr tcpdump iperf whois host dnsutils siege
```
Cette commande installe plusieurs outils utiles pour le diagnostic réseau et les tests de performance, notamment : traceroute, mtr, tcpdump, iperf, whois, host, dnsutils et siege.
 
```bash
iperf -s # Mode serveur
```
Cette commande lance iperf en mode serveur, ce qui permet à l'application d'accepter les connexions entrantes des clients pour tester les performances du réseau.
 
```bash
iperf -c us-test-01.europe-west4-b # Mode client
```
Cette commande lance iperf en mode client et établit une connexion avec le serveur spécifié à l'adresse "us-test-01.europe-west4-b". Elle est utilisée pour mesurer les performances du réseau entre le client (cette machine) et le serveur.
 
```bash
gcloud compute instances create us-test-04 \
--subnet subnet-europe-west4 \
--zone europe-west4-c \
--tags ssh,http
```
Cette commande crée une nouvelle instance de calcul nommée "us-test-04" dans la zone "europe-west4-c" et la place dans le sous-réseau "subnet-europe-west4". L'instance sera marquée avec les balises "ssh" et "http", ce qui permettra de contrôler les règles de pare-feu et d'autres configurations réseau.
 
```bash
sudo apt-get update
```
Cette commande met à jour la liste des paquets disponibles dans les référentiels configurés sur le système.
 
```bash
sudo apt-get -y install traceroute mtr tcpdump iperf whois host dnsutils siege
```
Cette commande installe plusieurs utilitaires réseau utiles sur le système.
```bash
iperf -s -u # Mode serveur
```
Cette commande exécute iperf en mode serveur pour mesurer les performances du réseau en utilisant le protocole UDP.
 
```bash
iperf -c us-test-02.us-east1-d -u -b 2G # Mode client
```
Cette commande exécute iperf en mode client pour mesurer les performances du réseau en envoyant un trafic de 2 Gbits/s vers l'hôte "us-test-02.us-east1-d" en utilisant le protocole UDP.
 
```bash
iperf -s # Mode serveur
```
Cette commande exécute iperf en mode serveur pour mesurer les performances du réseau.
 
```bash
iperf -c us-test-01.europe-west4-b -P 20 # Mode client
```
Cette commande exécute iperf en mode client avec 20 threads simultanés pour mesurer les performances du réseau vers l'adresse "us-test-01.europe-west4-b".
 

#Terraform


La commande mentionnée, 'gcloud auth list', présente la liste des comptes Google Cloud qui sont authentifiés localement sur la machine. Elle permet de visualiser les comptes actuellement connectés et autorisés pour exécuter des opérations via gcloud.

De même, 'gcloud config list project' affiche le projet Google Cloud configuré et utilisé par les commandes gcloud. Cette commande offre une vérification rapide du projet défini comme projet par défaut pour les opérations avec gcloud.

En bash, 'touch instance.tf' crée un fichier vide nommé 'instance.tf'.

## Fichier instance.tf

```bash
resource "google_compute_instance" "terraform" {
  project      = "qwiklabs-gcp-01-8ee549b02b4f"
  name         = "terraform"
  machine_type = "e2-medium"
  zone         = "us-central1-b"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
}
```
# Terraform-init

La commande "terraform init" prépare un nouveau répertoire de travail pour l'utilisation de Terraform. Elle met en place les plugins et modules requis pour la gestion de l'infrastructure décrite dans les fichiers de configuration Terraform, permettant ainsi une configuration adéquate du projet.

```bash
terraform init
```

# Terraform Plan

La commande "terraform plan" génère un plan d'exécution Terraform en analysant les fichiers de configuration présents dans le répertoire de travail. Elle présente un résumé des actions que Terraform effectuera pour mettre en place l'infrastructure décrite. Cette fonctionnalité permet de visualiser les modifications potentielles avant de les appliquer, offrant ainsi une opportunité de validation préalable.

```bash
terraform plan
```

```bash
Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # google_compute_instance.terraform will be created
  + resource "google_compute_instance" "terraform" {
      + can_ip_forward       = false
      + cpu_platform         = (known after apply)
      + current_status       = (known after apply)
      + deletion_protection  = false
      + effective_labels     = (known after apply)
      + guest_accelerator    = (known after apply)
      + id                   = (known after apply)
      + instance_id          = (known after apply)
      + label_fingerprint    = (known after apply)
      + machine_type         = "e2-medium"
      + metadata_fingerprint = (known after apply)
      + min_cpu_platform     = (known after apply)
      + name                 = "terraform"
      + project              = "qwiklabs-gcp-01-8ee549b02b4f"
      + self_link            = (known after apply)
      + tags_fingerprint     = (known after apply)
      + terraform_labels     = (known after apply)
      + zone                 = "us-central1-b"

      + boot_disk {
          + auto_delete                = true
          + device_name                = (known after apply)
          + disk_encryption_key_sha256 = (known after apply)
          + kms_key_self_link          = (known after apply)
          + mode                       = "READ_WRITE"
          + source                     = (known after apply)

          + initialize_params {
              + image                  = "debian-cloud/debian-11"
              + labels                 = (known after apply)
              + provisioned_iops       = (known after apply)
              + provisioned_throughput = (known after apply)
              + size                   = (known after apply)
              + type                   = (known after apply)
            }
        }

      + network_interface {
          + internal_ipv6_prefix_length = (known after apply)
          + ipv6_access_type            = (known after apply)
          + ipv6_address                = (known after apply)
          + name                        = (known after apply)
          + network                     = "default"
          + network_ip                  = (known after apply)
          + stack_type                  = (known after apply)
          + subnetwork                  = (known after apply)
          + subnetwork_project          = (known after apply)

          + access_config {
              + nat_ip       = (known after apply)
              + network_tier = (known after apply)
            }
        }
    }

Plan: 1 to add, 0 to change, 0 to destroy.
```

# Terraform apply

La commande "terraform apply" est employée pour exécuter les changements définis dans les fichiers de configuration Terraform. Elle opère la création, la mise à jour ou la suppression des ressources conformément aux ajustements apportés dans le code Terraform depuis la dernière exécution. Avant d'appliquer ces changements, Terraform fournit un aperçu des actions planifiées et sollicite une confirmation préalable à leur mise en œuvre.

```bash
terraform apply
```

# Terraform Show

La commande "terraform show" présente l'état courant de l'infrastructure gérée par Terraform. Elle expose les ressources existantes avec leurs attributs et valeurs actuels. Cette fonctionnalité est pratique pour inspecter l'état actuel de l'infrastructure après l'exécution des modifications.

```bash
terraform show
```

```bash
student_03_ebd1b870cab4@cloudshell:~ (qwiklabs-gcp-01-8ee549b02b4f)$ terraform show
# google_compute_instance.terraform:
resource "google_compute_instance" "terraform" {
    can_ip_forward       = false
    cpu_platform         = "Intel Broadwell"
    current_status       = "RUNNING"
    deletion_protection  = false
    effective_labels     = {}
    enable_display       = false
    guest_accelerator    = []
    id                   = "projects/qwiklabs-gcp-01-8ee549b02b4f/zones/us-central1-b/instances/terraform"
    instance_id          = "4721062745358440082"
    label_fingerprint    = "42WmSpB8rSM="
    machine_type         = "e2-medium"
    metadata_fingerprint = "DbMN_P0pNmQ="
    name                 = "terraform"
    project              = "qwiklabs-gcp-01-8ee549b02b4f"
    self_link            = "https://www.googleapis.com/compute/v1/projects/qwiklabs-gcp-01-8ee549b02b4f/zones/us-central1-b/instances/terraform"
    tags_fingerprint     = "42WmSpB8rSM="
    terraform_labels     = {}
    zone                 = "us-central1-b"

    boot_disk {
        auto_delete = true
        device_name = "persistent-disk-0"
        mode        = "READ_WRITE"
        source      = "https://www.googleapis.com/compute/v1/projects/qwiklabs-gcp-01-8ee549b02b4f/zones/us-central1-b/disks/terraform"

        initialize_params {
            enable_confidential_compute = false
            image                       = "https://www.googleapis.com/compute/v1/projects/debian-cloud/global/images/debian-11-bullseye-v20240110"
            labels                      = {}
            provisioned_iops            = 0
            provisioned_throughput      = 0
            size                        = 10
            type                        = "pd-standard"
        }
    }
    
    network_interface {
        internal_ipv6_prefix_length = 0
        name                        = "nic0"
        network                     = "https://www.googleapis.com/compute/v1/projects/qwiklabs-gcp-01-8ee549b02b4f/global/networks/default"
        network_ip                  = "10.128.0.2"
        queue_count                 = 0
        stack_type                  = "IPV4_ONLY"
        subnetwork                  = "https://www.googleapis.com/compute/v1/projects/qwiklabs-gcp-01-8ee549b02b4f/regions/us-central1/subnetworks/default"
        subnetwork_project          = "qwiklabs-gcp-01-8ee549b02b4f"

        access_config {
            nat_ip       = "35.225.128.159"
            network_tier = "PREMIUM"
        }
    }

    scheduling {
        automatic_restart   = true
        min_node_cpus       = 0
        on_host_maintenance = "MIGRATE"
        preemptible         = false
        provisioning_model  = "STANDARD"
    }

    shielded_instance_config {
        enable_integrity_monitoring = true
        enable_secure_boot          = false
        enable_vtpm                 = true
    }
}
```
